import Ember from 'ember'

export default Ember.Controller.extend
(
  { isValid: Ember.computed
    ( 'model.description'
    , 'model.duration'
    , { get()
        { return ! Ember.isEmpty ( this.get ( 'model.description'  ) )
              && ! Ember.isEmpty ( this.get ( 'model.duration'     ) )
        }
      }
    )
  , errorMessage: ''

  , actions:
    { save ()
      { if ( this.get ( 'isValid' ) )
        { return true
        }
        else
        { this.set ( 'errorMessage', 'Missing fields' )
          return false
        }
      }
    }
  }
)


