import Ember from 'ember'

export default Ember.Controller.extend
(
  { isValid: Ember.computed
    ( 'model.title'
    , 'model.client'
    , 'model.email'
    , { get ()
        { return ! Ember.isEmpty ( this.get ( 'model.title'  ) )
              && ! Ember.isEmpty ( this.get ( 'model.client' ) )
              && ! Ember.isEmpty ( this.get ( 'model.email'  ) )
        }
      }
    )

  , errorMessage: ''

  , actions :
    { save ()
      { if ( this.get ( 'isValid' ) )
        { this.get ( 'model' )
          .save ()
          .then
          ( ( project ) => 
            { this.transitionToRoute
              ( 'actions'
              , project
              )
            }
          )
        }
        else
        { this.set ( 'errorMessage', 'Missing fields' )
        }
        return false
      }

    , cancel()
      { return true
      }
    }
  }
)
