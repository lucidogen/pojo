import Base from './base'

export default Base.extend(
  { actions :
    { cancel ()
      { this.transitionToRoute
        ( 'actions'
        , this.get ( 'model' )
        )
        return false
      }
    }
  }
)


