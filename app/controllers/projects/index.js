import Ember from 'ember'

export default Ember.Controller.extend
(
  { possibleStates:
    [ 'open'
    , 'closed'
    ]
  }
)
