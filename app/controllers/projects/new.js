import Base from './base'

export default Base.extend
(
  { actions :
    { cancel ()
      { this.transitionToRoute
        ( 'projects' )
        return false
      }
    }
  }
)

