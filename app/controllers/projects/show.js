import Ember from 'ember'

export default Ember.Controller.extend(
  { actions:
    { edit() {
        this.transitionToRoute('projects.edit', this.get('model'))
        return false
      }
    }
  }
)


