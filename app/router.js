import Ember from 'ember'
import config from './config/environment'

var Router = Ember.Router.extend
( { location: config.locationType
  }
)

Router.map
( function ()
  { this.route
    ( 'projects'
    , function ()
      { this.route
        ( 'new' )

        this.route
        ( 'edit'
        , { path: ':project_id/edit' }
        )

        this.route
        ( 'show'
        , { path: ':project_id' }
        , function ()
          { this.route
            ( 'actions'
            , { resetNamespace: true }
            , function ()
              {
                this.route
                ( 'new' )

                this.route
                ( 'edit'
                , { path: ':action_id/edit' }
                )
              }
            )
          }
        )
      }
    )
  }
)

export default Router

