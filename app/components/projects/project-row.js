import Ember from 'ember'

export default Ember.Component.extend
( { tagName: 'tr'
  , project:       null  // passed-in
  , projectStates: null  // passed-in
  , actions:
    { saveProject ()
      { let project = this.get ( 'project' )

        if ( project.get ( 'hasDirtyAttributes' ) )
        { this.sendAction ( 'save', project )
        }
      }
    }
  }
)
