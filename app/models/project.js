import DS from 'ember-data'
import { Model } from 'ember-pouch'

export default Model.extend
(
 { title:       DS.attr    ( 'string'  )
 , description: DS.attr    ( 'string'  )
 , ceatedOn:    DS.attr    ( 'date'    )
 , client:      DS.attr    ( 'string'  )
 , email:       DS.attr    ( 'string'  )
 , phone:       DS.attr    ( 'string'  )
 , state:       DS.attr    ( 'string'  )
 , actions:     DS.hasMany ( 'action'  )
 }
)
