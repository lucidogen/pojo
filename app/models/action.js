import DS from 'ember-data'
import { Model } from 'ember-pouch'

export default Model.extend
(
 { date:        DS.attr      ( 'date'    )
 , description: DS.attr      ( 'string'  )
 , duration:    DS.attr      
   ( 'number'
   , { defaultValue: 0 }
   )
 , project:     DS.belongsTo ( 'project' )
 }
)
