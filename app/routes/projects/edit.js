import Ember from 'ember'

export default Ember.Route.extend
( // Reset unsaved changes
  { resetController ( controller, isExiting )
    { if ( isExiting )
      { let model = controller.get( 'model' )
        // no need to check for hasDirtyAttributes
        model.rollback ()
      }
    }
  }
)
