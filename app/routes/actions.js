import Ember from 'ember'

export default Ember.Route.extend
(
  { actions:
    { save ()
      { this.modelFor ( 'actions/edit' )
        .save ()
        .then
        ( () =>
          { this.transitionTo ( 'actions' )
          }
        )
      }

    , delete ( action )
      {
        let project = this.modelFor ( 'projects/show' )
        
        project.get ( 'actions' )
        .removeObjects ( action )

        project.save ()
        .then
        ( () =>
          { action.destroyRecord ()
            .then
            ( () =>
              { this.transitionTo ( 'actions' )
              }
            )
          }
        )
        return false
      }
    }
  }
)
