import Ember from 'ember'

export default Ember.Route.extend
( { model ( params )
    { return this.store
      .findRecord ( 'action', params.action_id )
    }

    // Reset unsaved changes
  , resetController ( controller, isExiting )
    { if ( isExiting )
      { let model = controller.get( 'model' )
        // no need to check for hasDirtyAttributes
        model.rollback ()
      }
    }
  }
)

