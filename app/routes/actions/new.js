import Ember from 'ember'

export default Ember.Route.extend
( { model()
    { return this.store.createRecord
      ( 'action'
      , { project: this.modelFor ( 'projects/show' )
        , date: new Date()
        }
      )
    }

    // Remove new records when we leave with 'Cancel'
  , resetController ( controller, isExiting )
    { if ( isExiting )
      { let model = controller.get( 'model' )
        if ( model.get ( 'isNew' ) )
        { model.destroyRecord ()
        }
      }
    }
    
  , actions:
    { save ()
      { let action  = this.modelFor ( 'actions/new' )
        let project = this.modelFor ( 'projects/show' )
        
        console.log ( action.get ( 'date' ) )

        action.save ()
        .then
        ( (a) =>
          { project.get ( 'actions' )
            .pushObject ( a )
            project.save ()
            .then
            ( () =>
              { this.transitionTo ( 'actions' )
              }
            )
          }
        )
      }
    , cancel ()
      { this.transitionTo ( 'actions' )
      }
    }
  }
)

