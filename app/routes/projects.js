import Ember from 'ember'

export default Ember.Route.extend
(
  { actions:
    { delete ( model )
      {
        model.destroyRecord ()
        .then
        ( () =>
          { this.transitionTo ( 'projects.index' )
          }
        )
        return false
      }

    , new ()
      {
        this.transitionTo ( 'projects.new' )
        return false
      }
    }
  }
)
